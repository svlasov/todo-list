#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module Docstring
"""
from __future__ import print_function, division

from click import echo
from requests import put, get


if __name__ == "__main__":
    put('http://localhost:5000/todos/todo1', data={'task': 'Remember the milk'}).json()
    resp = get('http://localhost:5000/todos')
    doc = resp.json()
    echo(doc)