import copy

from flask import Flask
from flask_restful import reqparse, abort, Api, Resource

app = Flask(__name__)
api = Api(app)

TODO_RAW_DATA = {'todo1':'build an API',
                 'todo2': '?????',
                 'todo3': 'profit!'}

class TODOS_DB:

    def __init__(self, initial_data):
        self._data = list(initial_data.values())
        self._index = copy.deepcopy(initial_data)

    def __contains__(self, item):
        return item in self._index

    def __getitem__(self, item):
        return self._index[item]

    def __setitem__(self, key, value):
        task = value['task'] #{'task': value['task']}
        self._data.append(task)
        self._index[key] = task

    def __delitem__(self, key):
        v = self._index.pop(key, None)

        if v:
            self._data.pop(v)

        return v

    def keys(self):
        return self._index.keys()

    # put = push

TODOS = TODOS_DB(TODO_RAW_DATA)


def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in TODOS:
        abort(404, message="Todo {} doesn't exist".format(todo_id))

parser = reqparse.RequestParser()
parser.add_argument('task')


# Todo
# shows a single todo item and lets you delete a todo item
class Todo(Resource):
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        return TODOS[todo_id]

    def delete(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        del TODOS[todo_id]
        return '', 204

    def put(self, todo_id):
        args = parser.parse_args()

        TODOS[todo_id] = args#{'task': task}
        task = TODOS[todo_id]

        return task, 201


# TodoList
# shows a list of all todos, and lets you POST to add new tasks
class TodoList(Resource):
    def get(self):
        return TODOS._data

    def post(self):
        args = parser.parse_args()
        todo_id = int(max(TODOS.keys()).lstrip('todo')) + 1
        todo_id = 'todo{}'.format(todo_id)
        TODOS[todo_id] = args  # {'task': task}
        task = TODOS[todo_id]
        return task, 201

##
## Actually setup the Api resource routing here
##
api.add_resource(TodoList, '/todos')
api.add_resource(Todo, '/todos/<todo_id>')


if __name__ == '__main__':
    app.run(debug=True)
